﻿namespace CatsTestEngine
{
    partial class frmTestCase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLoginCase = new System.Windows.Forms.Button();
            this.btnGoToMenu = new System.Windows.Forms.Button();
            this.btnInsertData = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnLoginCase
            // 
            this.btnLoginCase.Location = new System.Drawing.Point(52, 47);
            this.btnLoginCase.Name = "btnLoginCase";
            this.btnLoginCase.Size = new System.Drawing.Size(215, 42);
            this.btnLoginCase.TabIndex = 0;
            this.btnLoginCase.Text = "1. Login Case";
            this.btnLoginCase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLoginCase.UseVisualStyleBackColor = true;
            this.btnLoginCase.Click += new System.EventHandler(this.btnLoginCase_Click);
            // 
            // btnGoToMenu
            // 
            this.btnGoToMenu.Location = new System.Drawing.Point(52, 95);
            this.btnGoToMenu.Name = "btnGoToMenu";
            this.btnGoToMenu.Size = new System.Drawing.Size(215, 42);
            this.btnGoToMenu.TabIndex = 1;
            this.btnGoToMenu.Text = "2. Media Penginapan";
            this.btnGoToMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGoToMenu.UseVisualStyleBackColor = true;
            this.btnGoToMenu.Click += new System.EventHandler(this.btnGoToMenu_Click);
            // 
            // btnInsertData
            // 
            this.btnInsertData.Location = new System.Drawing.Point(52, 143);
            this.btnInsertData.Name = "btnInsertData";
            this.btnInsertData.Size = new System.Drawing.Size(215, 42);
            this.btnInsertData.TabIndex = 2;
            this.btnInsertData.Text = "3. Insert Data";
            this.btnInsertData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInsertData.UseVisualStyleBackColor = true;
            this.btnInsertData.Click += new System.EventHandler(this.btnInsertData_Click);
            // 
            // frmTestCase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 384);
            this.Controls.Add(this.btnInsertData);
            this.Controls.Add(this.btnGoToMenu);
            this.Controls.Add(this.btnLoginCase);
            this.Name = "frmTestCase";
            this.Text = "Test Case Form";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLoginCase;
        private System.Windows.Forms.Button btnGoToMenu;
        private System.Windows.Forms.Button btnInsertData;
    }
}

