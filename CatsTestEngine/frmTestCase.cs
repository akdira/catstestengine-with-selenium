﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace CatsTestEngine
{
    public partial class frmTestCase : Form
    {

        ChromeDriver driver;

        public frmTestCase()
        {
            InitializeComponent();
        }

        private void btnLoginCase_Click(object sender, EventArgs e)
        {
            driver = new ChromeDriver();
            driver.Url = "http://localhost/cats2/";
            var usernameTag = driver.FindElementById("Login1_UserName");
            var passwordTag = driver.FindElementById("Login1_Password");
            var loginBtn = driver.FindElementById("Login1_LoginButton");

            usernameTag.Clear();
            usernameTag.SendKeys("13032984");

            Thread.Sleep(500);

            passwordTag.Clear();
            passwordTag.SendKeys("a");

            Thread.Sleep(500);

            loginBtn.Click();

            Thread.Sleep(1000);
        }

        private void btnGoToMenu_Click(object sender, EventArgs e)
        {
            driver.Url = "http://localhost/cats2/Pages/Administration/MediaPenginapanList.aspx";
        }

        private void btnInsertData_Click(object sender, EventArgs e)
        {
            var addBtn = driver.FindElementById("ctl00_Content_ButtonAdd");

            addBtn.Click();

            Thread.Sleep(1000);

            var guid = Guid.NewGuid().ToString();
            guid = guid.Replace("{", "").Replace("}", "").Replace("-", "");

            var txtName = driver.FindElementById("ctl00_Content_txtNamaMediaPenginapan");
            var btnSave = driver.FindElementById("ctl00_Content_PanelSubmit2_ButtonSubmit");

            txtName.SendKeys(guid);


            Thread.Sleep(500);

            btnSave.Click();


            Thread.Sleep(500);

            IAlert alert = driver.SwitchTo().Alert();
            alert.Accept();

            Thread.Sleep(1000);

            alert = driver.SwitchTo().Alert();
            alert.Accept();

        }
    }
}
